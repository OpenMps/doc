# OpenMps Documents

All Documents about OpenMPS are managed under [web pages](https://bitbucket.org/OpenMps/openmps.bitbucket.org/src/master/doc/).

They are web browsable from [OpenMPS's page](http://openmps.bitbucket.org/).

